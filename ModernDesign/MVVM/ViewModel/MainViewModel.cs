﻿using ModernDesign.Core;

namespace ModernDesign.MVVM.ViewModel
{
    class MainViewModel: ObservableObject
    {
        public RelayCommand HomeViewCommand { get; set; }
        public RelayCommand ContactViewcommand { get; set; }

        public HomeViewModel HomeVm { get; set; }
        public ContactViewModel ContactVm { get; set; }

        private object _currentView;

        public object CurrentView
        {
            get { return _currentView; }
            set 
            { 
                _currentView = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            HomeVm = new HomeViewModel();
            ContactVm = new ContactViewModel();
            CurrentView = HomeVm;

            HomeViewCommand = new RelayCommand(o => {
                CurrentView = HomeVm;
            });

            ContactViewcommand = new RelayCommand(o => {
                CurrentView = ContactVm;
            });
        }
    }
}
